import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import './App.css';
import * as serviceWorker from "./serviceWorker";
import Dashboard from "./components/Dashboard";
import FormLogin from "./components/FormLogin";
import FormRegister from "./components/FormRegister";
import PasswordReset from "./components/PasswordReset";
import Blocked from "./components/Blocked";
import Home from "./components/Home";
import BlankPage from "./components/BlankPage";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

const routing = (
  <Router>
    <div>
      <Switch>
        {/* <Route exact path="/" component={Home} /> */}
        <Route exact path="/" component={FormLogin} />
        <Route path="/Login" component={FormLogin} />
        <Route path="/Dashboard" component={Dashboard} />
        <Route path="/Register" component={FormRegister} />
        <Route path="/Reset" component={PasswordReset} />
        <Route path="/Home" component={Home} />
        <Route path="/BlankPage" component={BlankPage} />
        <Route  component={Blocked}/>
      </Switch>
    </div>
  </Router>
);
ReactDOM.render(routing, document.getElementById("root"));
