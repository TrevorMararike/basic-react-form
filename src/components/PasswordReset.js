import React, { Component } from "react";
// initial state
const initialState = {
  Email: ""
};
class PasswordReset extends Component {
  constructor(props) {
    super(props);

    this.state = {
      
      Email: ""
    };
  }

  // handle email onChange
  handleEmailChange = event => {
    this.setState({
      Email: event.target.value
    });
  };
  

  //handle  reset onChange
  handleSubmit = event => {
    event.preventDefault();
    
    if (!(this.state.Email === ""))
    {
    alert(`Follow link send to ${this.state.Email} to reset  password`);
      console.log(this.state);
    }
      //clear form
      this.setState(initialState);
    
    this.setState({
     Submit: event.target.value
    });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <h3>Welcome, please reset password</h3>
          <p>
            <input
              name="email"
              type="email"
              placeholder="Enter your email"
              value={this.state.Email}
              onChange={this.handleEmailChange}
              required
            />
          </p>
          
          <button type="submit">Reset</button>
        </div>
      </form>
    );
  }
}

export default PasswordReset;
