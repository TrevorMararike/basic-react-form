import React, { Component } from "react";
//set initial state
const initialState = {
  Email: "",
  Username: "",
  Password: "",
  PasswordConfirm: ""
 
};

class FormRegister extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Email: "",
      Username: "",
      Password: "",
      PasswordConfirm: ""
    };
  }
  // handle email onChange
  handleEmailChange = event => {
    this.setState({
      Email: event.target.value
    });
  };
  // handle name onChange
  handleUsernameChange = event => {
    this.setState({
      Username: event.target.value
    });
  };
  // handle  password onChange
  handlePasswordChange = event => {
    this.setState({
      Password: event.target.value
    });
  };
  // handle  passwordConfirm onChange
  handlePasswordConfirmChange = event => {
    this.setState({
      PasswordConfirm: event.target.value
    });
  };

  //handle  submit onChange
  handleSubmit = event => {
    event.preventDefault();

    // passwords not matching
    if (!(this.state.Password === this.state.PasswordConfirm)) {
      alert(`Hello, passwords do not match!`);
    }
    
     else
     // valid info has been entered, redirect user to login page
     {
      if (window.confirm('You have successfully registered!Click "ok" to login or Cancel to reload page ')) 
      {
      window.location.href='/Login';
      };
      console.log(this.state);
    }
    // store username and password for that session
    sessionStorage.setItem("email", this.state.Email);
    sessionStorage.setItem("password", this.state.Password);

    //clear form
    this.setState(initialState);
    this.setState({
      Submit: event.target.value
    });
  };

  render() {
    const { Email, Username, Password, PasswordConfirm } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <h3>Welcome, please register</h3>

          <p>
            <input
              name="username"
              type="text"
              placeholder="Enter your username"
              value={Username}
              onChange={this.handleUsernameChange}
              required
            />
          </p>
          <p>
            <input
              name="email"
              type="email"
              placeholder="Enter your email"
              value={Email}
              onChange={this.handleEmailChange}
              required
            />
          </p>
          <p>
            <input
              name="password"
              type="password"
              placeholder="Enter your password"
              value={Password}
              minLength="8"
              
              maxLength = "50"
              title="minimum of 8 characters"
              onChange={this.handlePasswordChange}
              required
            />
          </p>
          <p>
            <input
              name="passwordConfirm"
              type="password"
              placeholder="Confirm your password"
              value={PasswordConfirm}
              title="minimum of 8 characters"
              onChange={this.handlePasswordConfirmChange}
              required
              minLength = "8"
              maxLength = "50"
            />
          </p>
          <button type="submit">Register</button>
        </div>
      </form>
    );
  }
}

export default FormRegister;
