import React, { Component } from 'react';
import Table from './Table';

 class Dashboard extends Component {
  constructor(props){
    super(props);
    this.state={
      tableData:[
        {'fruit': 'Apple', 'cost': 100},
        {'fruit': 'Orange', 'cost': 50},
        {'fruit': 'Banana', 'cost': 35},
        {'fruit': 'Mango', 'cost': 70},
        {'fruit': 'Pineapple', 'cost': 45},
        {'fruit': 'Papaya', 'cost': 40},
        {'fruit': 'Watermelon', 'cost': 35}
      ]
    }}
    clickMe(){
    alert(`directing to blank page`)
  }
  render() {
    console.log(this.props)
    return (
      <div>
        <h1>Dashboard</h1>
        {/* <Table data={this.state.tableData}/> */}
        <a onClick ={this.clickMe.bind(this)} href ="/BlankPage">
        {<Table data={this.state.tableData}/>}
        </a>
      </div>
    )
  }
}

export default Dashboard
