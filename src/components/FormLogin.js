import React, { Component } from "react";

//set initial state
const initialState = {
  Email: "",
  Password: "",
  errorPassword: "",
  errorEmail: ""
};

let incIncorrectLogin = 1;

class FormLogin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Password: "",
      Email: ""
    };
  }
  // handle email onChange
  handleEmailChange = event => {
    this.setState({
      Email: event.target.value
    });
  };

  // handle  password onChange
  handlePasswordChange = event => {
    this.setState({
      Password: event.target.value
    });
  };
  //handle  submit onChange
  handleSubmit = event => {
    event.preventDefault();

    if((sessionStorage.getItem("email") === this.state.Email) && (sessionStorage.getItem("password") === this.state.Password)){
      incIncorrectLogin =0;
      this.props.history.push("/Dashboard");
    }else{
      alert("Incorrect login details\n" + (3 -incIncorrectLogin) + " attempt/s left");
      incIncorrectLogin += 1;
    }

    if(incIncorrectLogin > 3){
      this.props.history.push("/Blocked");
    }

    
    //clear form
    this.setState(initialState);

    this.setState({
      Submit: event.target.value
    });
  };
render() {
    const { Email, Password } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <h3>Welcome, please login</h3>
          <p>
            <input
              name="email"
              type="email"
              placeholder="Enter your email"
              value={Email}
              onChange={this.handleEmailChange}
              required
            />
          </p>

          <p>
            <input
              name="password"
              type="password"
              placeholder="Enter your password"
              value={Password}
              onChange={this.handlePasswordChange}
              minLength="8"
              maxLength="25"
              title="minimum of 8 characters"
              required
            />
          </p>
          <button type="submit" id="submit">
            Login
          </button>
        </div>

        <div>
          <p>
            <a href="/Reset" className="links">
              forgot password?
            </a>
          </p>
          <p>
            <a href="/Register" className="links">
              sign up
            </a>
          </p>
          
        </div>
      </form>
    );
  }
}

export default FormLogin;
